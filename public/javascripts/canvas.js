
//引数のBase64の文字列をBlob形式にしている
var base64ToBlob = function(base64){
	var base64Data = base64.split(',')[1], // Data URLからBase64のデータ部分のみを取得
		data = window.atob(base64Data), // base64形式の文字列をデコード
		buff = new ArrayBuffer(data.length),
		arr = new Uint8Array(buff),
		blob, i, dataLen;
	if(data.length == 0){
		return null;
	}

	// blobの生成
	for( i = 0, dataLen = data.length; i < dataLen; i++){
		arr[i] = data.charCodeAt(i);
	}
	blob = new Blob([arr], {type: 'image/jpeg'});
	return blob;
}



function img_send(){
		sendImageBinary();
}
