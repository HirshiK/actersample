package controllers;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import models.Actor;
import models.Customer;
import models.Details;
import models.Prefecture;
import models.Received;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import play.Play;
import play.data.DynamicForm;
import play.data.Form;
import play.data.FormFactory;
import play.i18n.Messages;
import play.mvc.Controller;
import play.mvc.Result;
import utils.DateParser;
import views.form.ActorForm;
import views.form.DetailsForm;
import views.form.ReceivedForm;
import views.html.create;
import views.html.detail;
import views.html.index;
import views.html.custom;
import views.html.neoindex;
import views.html.lcustom;
import views.html.upload;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.SqlRow;
import com.avaje.ebean.SqlUpdate;
import com.avaje.ebean.Transaction;
import com.avaje.ebean.TxCallable;

import play.mvc.Http.MultipartFormData;
import play.mvc.Http.MultipartFormData.FilePart;

public class Application extends Controller {
	private static final Logger logger = LoggerFactory
			.getLogger(Application.class);

	public Result index() {
		logger.info("Application#index");
		List<Received> received = Received.finder.all();
		return ok(index.render("O_RECEIVED_MASTER", received));
	}


	public Result neoindex() {
		logger.info("Application#neoindex");
		List<Customer> neocus = Customer.finder.all();
		String str = "";
		return ok(neoindex.render("CUSTOMER_MASTER", neocus,str));
	}

	public Result detail(Long ordernumber) {
		logger.info("Application#detail");
		Details details = Details.finder.byId(ordernumber);
		return ok(detail.render("Received Detail", details));
	}

	public Result custom(Long customernumber) {
		logger.info("Application#customl");
		Customer customer = Customer.finder.byId(customernumber);
		return ok(custom.render("Customer Detail", customer));
	}

	public Result lcustom(Long customernumber){
		logger.info("Application#last");
		com.avaje.ebean.ExpressionList<Received> elcustom= Received.finder.where().eq("customernumber",customernumber);
		List<Received> customer =  elcustom.findList();
		return ok(lcustom.render("lCustomer Detail", customer));
	}
	public Result uploader(){
		return ok(upload.render());
	}
	 public Result upload() {
		 play.mvc.Http.MultipartFormData body = request().body().asMultipartFormData();
		 play.mvc.Http.MultipartFormData.FilePart picture1 = body.getFile("picture1");
		 play.mvc.Http.MultipartFormData.FilePart picture2 = body.getFile("picture2");
		 play.mvc.Http.MultipartFormData.FilePart picture3 = body.getFile("picture3");

		 List<play.mvc.Http.MultipartFormData.FilePart> piclist = new ArrayList<>();


		 DynamicForm requestData = DynamicForm.form().bindFromRequest();
		    String txt1 = requestData.get("txt1");
		    String txt2 = requestData.get("txt2");

		    System.out.println("formtest:"+txt1+"_"+txt2);

		 if(picture1!=null){piclist.add(picture1);}
		 if(picture2!=null){piclist.add(picture2);}
		 if(picture3!=null){piclist.add(picture3);}


		 if (piclist != null) {
		    	for(int i=0;i<piclist.size();i++){
		    		File upfile = null;
		    		try{
		    			 upfile = utils.Filetmp.filetmp(piclist.get(i),String.valueOf(i));
		    			 System.out.println(i+"番目："+upfile.length());
		    			 utils.S3upload.s3Upload(upfile,String.valueOf(i));

				      }catch(IOException e){
				    	  e.printStackTrace();
				      }

		    		 upfile.delete();
		    	}
		    	System.out.println("up_kanryou");
		    	return ok("File uploaded");
		      }else{
		    	  flash("error", "Missing file");
		    	  return redirect(routes.Application.index());
		    }
	 }


//	public Result create() {
//		logger.info("Application#create");
//		ReceivedForm form = new ReceivedForm();
//		Form<ReceivedForm> formData = Form.form(ReceivedForm.class).fill(form);
//		return ok(create.render("Actor Create", formData));
//	}

//	public Result detailsCreate() {
//		logger.info("Application#create");
//		DetailsForm dform = new DetailsForm();
//		Form<DetailsForm> dformData = Form.form(DetailsForm.class).fill(dform);
//		return ok(detailsCreate.render("Actor Create", dformData));
//	}

//	public Result save() {
//		logger.info("Application#received_save");
//		Form<ReceivedForm> formData = Form.form(ReceivedForm.class)
//				.bindFromRequest();
//		if (formData.hasErrors()) {
//			flash("error", Messages.get("actor.validation.error"));
//			return badRequest(create.render("retry", formData));
//		} else {
//			Received received = Received.convertToModel(formData.get());
//			Ebean.execute(() -> {
//				SqlRow row = Ebean.createSqlQuery(
//						"SELECT MAX(id) AS cnt FROM received").findUnique();
//				Long cnt = row.getLong("cnt");
//				received.ordernumber = cnt == null ? 1L : (cnt + 1L);
//				received.save();
//			});
//			flash("success", Messages.get("actor.save.success"));
//		}
//		return redirect(routes.Application.index());
//	}


	// public Result delete(Long id) {
	// logger.info("Application#delete");
	// if (id == null || id == 0L) {
	// flash("error", Messages.get("actor.validation.error"));
	// List<Actor> actor = Actor.finder.all();
	// return badRequest(index.render("Actor List", actor));
	// } else {
	// Boolean result = Ebean.execute(new TxCallable<Boolean>() {
	// @Override
	// public Boolean call() {
	// Actor actor = Actor.finder.byId(id);
	// actor.delete();
	// return Boolean.TRUE;
	// }
	// });
	// if (result) {
	// flash("success", Messages.get("actor.delete.success"));
	// } else {
	// flash("error", Messages.get("actor.delete.error"));
	// }
	// }
	// return redirect(routes.Application.index());
	// }

	public Result init() {
		logger.info("Application#init");

		try (Transaction txn = Ebean.beginTransaction()) {

			SqlUpdate actorAllDelete = Ebean
					.createSqlUpdate("DELETE FROM received");
			actorAllDelete.execute();

			txn.setBatchMode(true);
			txn.setBatchSize(10);
			txn.setBatchGetGeneratedKeys(false);


			new Received(1000L, "1234567890", "02050",
					DateParser.parse("2013-01-01"),
					DateParser.parse("2013-01-05"), "50000").save();
			new Received(3000L, "1111111111", "01111",
					DateParser.parse("2013-01-01"),
					DateParser.parse("2013-01-05"), "150000").save();
			new Received(2000L, "1111111111", "01111",
					DateParser.parse("2013-01-01"),
					DateParser.parse("2013-01-05"), "150000").save();

//			new Received(1000L, 1234567890L, "02050",
//					DateParser.parse("2013-01-01"),
//					DateParser.parse("2013-01-05"), "50000").save();
//			new Received(3000L, 1111111111L, "01111",
//					DateParser.parse("2013-01-01"),
//					DateParser.parse("2013-01-05"), "150000").save();
//			new Received(2000L, 1111111111L, "01111",
//					DateParser.parse("2013-01-01"),
//					DateParser.parse("2013-01-05"), "150000").save();

			System.out.println("a");

			SqlUpdate detailsAllDelete = Ebean
					.createSqlUpdate("DELETE FROM details");
			detailsAllDelete.execute();

			new Details(1000L, "1234567890", 400, 4).save();
			new Details(3L, "1234567890", 500, 5).save();


//			SqlUpdate customerAllDelete = Ebean
//					.createSqlUpdate("DELETE FROM custom");
//			customerAllDelete.execute();

			new Customer(1234567890L, "AAA","相田","1112222","tokyo","0311112222").save();
			new Customer(1111111111L, "BBB","井上","1112222","osaka","0311112222").save();
			new Customer(2222222222L, "CCC","上田","1112222","gunma","0311112222").save();

			// new Actor( 1L, "丹波哲郎", 175, "O", DateParser.parse("1922-07-17"),
			// 13,"m").save();
			// new Actor( 2L, "森田健作", 175, "O", DateParser.parse("1949-12-16"),
			// 13,"m").save();
			// new Actor( 3L, "加藤剛", 173, null, DateParser.parse("1949-12-16"),
			// 22,"m").save();
			// new Actor( 4L, "島田陽子", 171, "O", DateParser.parse("1953-05-17"),
			// 43,"f").save();
			// new Actor( 5L, "山口果林", null, null,
			// DateParser.parse("1947-05-10"), 13,"f").save();
			// new Actor( 6L, "佐分利信", null, null,
			// DateParser.parse("1909-02-12"), 1,"m").save();
			// new Actor( 7L, "緒方拳", 173, "B", DateParser.parse("1937-07-20"),
			// 13,"m").save();
			// new Actor( 8L, "松山政路", 167, "A", DateParser.parse("1947-05-21"),
			// 13,"m").save();
			// new Actor( 9L, "加藤嘉", null, null, DateParser.parse("1913-01-12"),
			// 13,"m").save();
			// new Actor(10L, "菅井きん", 155, "B", DateParser.parse("1926-02-28"),
			// 13,"f").save();
			// new Actor(11L, "笠智衆", null, null, DateParser.parse("1904-05-13"),
			// 43,"m").save();
			// new Actor(12L, "殿山泰司", null, null,
			// DateParser.parse("1915-10-17"), 28,"m").save();
			// new Actor(13L, "渥美清", 173, "B", DateParser.parse("1928-03-10"),
			// 13,"m").save();
			//
			// SqlUpdate prefectureAllDelete =
			// Ebean.createSqlUpdate("DELETE FROM prefecture");
			// prefectureAllDelete.execute();

			// new Prefecture( 1, "北海道").save();
			// new Prefecture( 2, "青森県").save();
			// new Prefecture( 3, "岩手県").save();
			// new Prefecture( 4, "宮城県").save();
			// new Prefecture( 5, "秋田県").save();
			// new Prefecture( 6, "山形県").save();
			// new Prefecture( 7, "福島県").save();
			// new Prefecture( 8, "茨城県").save();
			// new Prefecture( 9, "栃木県").save();
			// new Prefecture(10, "群馬県").save();
			// new Prefecture(11, "埼玉県").save();
			// new Prefecture(12, "千葉県").save();
			// new Prefecture(13, "東京都").save();
			// new Prefecture(14, "神奈川県").save();
			// new Prefecture(15, "新潟県").save();
			// new Prefecture(16, "富山県").save();
			// new Prefecture(17, "石川県").save();
			// new Prefecture(18, "福井県").save();
			// new Prefecture(19, "山梨県").save();
			// new Prefecture(20, "長野県").save();
			// new Prefecture(21, "岐阜県").save();
			// new Prefecture(22, "静岡県").save();
			// new Prefecture(23, "愛知県").save();
			// new Prefecture(24, "三重県").save();
			// new Prefecture(25, "滋賀県").save();
			// new Prefecture(26, "京都府").save();
			// new Prefecture(27, "大阪府").save();
			// new Prefecture(28, "兵庫県").save();
			// new Prefecture(29, "奈良県").save();
			// new Prefecture(30, "和歌山県").save();
			// new Prefecture(31, "鳥取県").save();
			// new Prefecture(32, "島根県").save();
			// new Prefecture(33, "岡山県").save();
			// new Prefecture(34, "広島県").save();
			// new Prefecture(35, "山口県").save();
			// new Prefecture(36, "徳島県").save();
			// new Prefecture(37, "香川県").save();
			// new Prefecture(38, "愛媛県").save();
			// new Prefecture(39, "高知県").save();
			// new Prefecture(40, "福岡県").save();
			// new Prefecture(41, "佐賀県").save();
			// new Prefecture(42, "長崎県").save();
			// new Prefecture(43, "熊本県").save();
			// new Prefecture(44, "大分県").save();
			// new Prefecture(45, "宮崎県").save();
			// new Prefecture(46, "鹿児島県").save();
			// new Prefecture(47, "沖縄県").save();

			txn.commit();

		} catch (Exception e) {
			System.out.println(e.getStackTrace());
			System.out.println("aaa");
		}

		return redirect(routes.Application.index());
	}

	public Result error500(Integer f) {
		if (f == 1) {
			throw new RuntimeException("Internal Server Error");
		}
		return ok();
	}

}
