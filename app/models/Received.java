package models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.apache.commons.lang3.StringUtils;

import utils.DateParser;
import views.form.ReceivedForm;

import com.avaje.ebean.Model;

@Entity(name = "O_RECEIVED_MASTER")
public class Received extends Model {
	@Id
	@Column(name = "ORDERNUMBER")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Size(min = 1, max = 10)
	public Long ordernumber;
	@Column(name = "CUSTOMERNUMBER")
	@NotNull
	public String customernumber;
	@Column(name = "EMPLOYEENUMBER")
	@NotNull
	public String employeenumber;
	@Column(name = "ORDERSDATE")
	@NotNull
	public Date ordersdate;
	@Column(name = "DELIVERY")
	@NotNull
	public Date delivery;
	@Column(name = "FARE")
	@NotNull
	public String fare;

	public Received() {
	}

	public Received(Long ordernumber, String customernumber,
			String employeenumber, Date ordersdate, Date delivery, String fare) {
		this.ordernumber = ordernumber;
		this.customernumber = customernumber;
		this.employeenumber = employeenumber;
		this.ordersdate = ordersdate;
		this.delivery = delivery;
		this.fare = fare;
	}

	public static Find<Long, Received> finder = new Find<Long, Received>() {
	};

	public static Received convertToModel(ReceivedForm form) {
		Received received = new Received();
		received.ordernumber = StringUtils.isNotEmpty(form.ordernumber) ? Long.valueOf(form.ordernumber)
				: null;
		received.customernumber = form.customernumber;
//		received.customernumber = StringUtils.isNotEmpty(form.customernumber) ? Long.valueOf(form.customernumber) : null;
		received.employeenumber = form.employeenumber;
		received.ordersdate = StringUtils.isNotEmpty(form.ordersdate) ? DateParser
				.parse(form.ordersdate) : null;
		received.delivery = StringUtils.isNotEmpty(form.delivery) ? DateParser
				.parse(form.delivery) : null;
		received.fare = form.fare;
		return received;
	}

	public static ReceivedForm convertToForm(Received received) {
		ReceivedForm form = new ReceivedForm();
		form.ordernumber = received.ordernumber.toString();
		form.customernumber = received.customernumber.toString();
		form.employeenumber = received.employeenumber != null ? received.employeenumber.toString() : null;
		form.ordersdate = received.ordersdate != null ? DateParser
				.format(received.ordersdate) : null;
		form.delivery = received.delivery != null ? received.delivery
				.toString() : null;
		form.fare = received.fare;
		return form;
	}

	@Override
	public String toString() {
		return "Received [ordernumber=" + ordernumber + ", customernumber="
				+ customernumber + ", employeenumbert=" + employeenumber
				+ ", ordersdate=" + ordersdate + ",deliveryt=" + delivery
				+ ",fare=" + fare + "]";
	}

}
