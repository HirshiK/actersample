package models;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Version;

import org.jetbrains.annotations.NotNull;

import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.CreatedTimestamp;

@Entity
public class Parent extends Model {
	@Id
	public Long id;
	@NotNull
	public String name;

	@CreatedTimestamp
	public Date createDate;

	@Version
	public Date updateDate;

	public String toString() {
		return "Parent [id =" + id + ",name" + name + ",createDate="
				+ createDate + ",updateDate=" + "]";
	}

}
