package models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.avaje.ebean.Model;

@Entity(name = "customer")
public class Customer extends Model {
	@Id
	@Column(name = "CUSTOMERNUMBER")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Size(min = 1, max = 10)
	public Long customerenumber;
	@Column(name = "COMPANYNAME")
	@NotNull
	public String companyname;
	@Column(name = "PARSONNAME")
	@NotNull
	public String parsonname;
	@Column(name = "ZIPCODE")
	@NotNull
	public String zipcode;
	@Column(name = "ADDRESS")
	@NotNull
	public String address;
	@Size(min = 1, max = 11)
	@Column(name = "PHONENUMBER")
	public String phonenumber;

	public Customer() {
	}

	public Customer(Long customerenumber, String companyname,
			String parsonname, String zipcode, String address,
			String phonenumber) {
		this.customerenumber = customerenumber;
		this.companyname = companyname;
		this.parsonname = parsonname;
		this.zipcode = zipcode;
		this.address = address;
		this.phonenumber = phonenumber;
	}

	public static Find<Long, Customer> finder = new Find<Long, Customer>() {
	};

	// public static Customer convertToModel(DetailsForm form) {
	// Customer customer = new Customer();
	// customer.customernumber = StringUtils.isNotEmpty(form.customernumber) ?
	// Long.valueOf(form.customernumber)
	// : null;
	// customer.address=form.address;
	// customer.companyname=form.companyname;
	//
	// return customer;
	// }

	// public static DetailsForm convertToForm(Details details) {
	// DetailsForm form = new DetailsForm();
	// form.ordernumber = details.ordernumber.toString();
	// form.customernumber = details.customernumber;
	// form.unit =details.unit != null ? details.unit.toString() : null;
	// form.quantity = details.quantity != null ? details.quantity.toString() :
	// null;
	// return form;
	// }

	@Override
	public String toString() {
		return "Customer [customernumber=" + customerenumber + ",companyname="
				+ companyname + ",parsonname=" + parsonname + ",zipcode="
				+ zipcode + ",address=" + address + ",phonenumber="
				+ phonenumber + "]";
	}

}