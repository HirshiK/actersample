package models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.apache.commons.lang3.StringUtils;

import views.form.DetailsForm;

import com.avaje.ebean.Model;

@Entity(name = "details")
public class Details extends Model {
	@Id
	@Column(name = "ORDERNUMBER")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Size(min = 1, max = 10)
	public Long ordernumber;
	@Column(name = "PRODUCT")
	@NotNull
	public String product;
	@Column(name = "UNIT")
	@NotNull
	public int unit;
	@Column(name ="QUANTITY")
	@NotNull
	public int quantity;

	public Details() {
	}

	public Details(Long ordernumber, String product,
			int unit ,int quantity) {
		this.ordernumber = ordernumber;
		this.product = product;
		this.unit = unit;
		this.quantity = quantity;
	}

	public static Find<Long, Details> finder = new Find<Long, Details>() {
	};

	public static Details convertToModel(DetailsForm form) {
		Details details = new Details();
		details.ordernumber = StringUtils.isNotEmpty(form.ordernumber) ? Long.valueOf(form.ordernumber)
				: null;
		details.product = form.product;
		details.unit =  StringUtils.isNotEmpty(form.unit) ? Integer.valueOf(form.unit) : null;
		details.quantity =  StringUtils.isNotEmpty(form.quantity) ? Integer.valueOf(form.quantity) : null;
		return details;
	}

//	public static DetailsForm convertToForm(Details details) {
//		DetailsForm form = new DetailsForm();
//		form.ordernumber = details.ordernumber.toString();
//		form.customernumber = details.customernumber;
//		form.unit =details.unit != null ? details.unit.toString() : null;
//		form.quantity = details.quantity != null ? details.quantity.toString() : null;
//		return form;
//	}

	@Override
	public String toString() {
		return "Details [ordernumber=" + ordernumber + ", product="
				+ product + ", unit=" + unit
				+ ",quantity=" + quantity + "]";
	}

}
