package views.form;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import play.data.validation.ValidationError;
import play.i18n.Messages;

public class DetailsForm {
	public String ordernumber = "";
	public String product = "";
	public String unit = "";
	public String quantity = "";

	public DetailsForm() {

	}

	public DetailsForm(String ordernumber, String product, String unit,
			String quantity) {
		this.ordernumber = ordernumber;
		this.product = product;
		this.unit = unit;
		this.quantity = quantity;
	}

	public List<ValidationError> validate() {
		List<ValidationError> errors = new ArrayList<ValidationError>();

		if (ordernumber == null || ordernumber.length() == 0) {
			errors.add(new ValidationError("ordernumber", Messages
					.get("details.ordernumber.required")));
		}
		if (product == null || product.length() == 0) {
			errors.add(new ValidationError("height", Messages
					.get("details.product.required")));
		}
		if (StringUtils.isNotEmpty(unit)) {
			Integer tmpB = Integer.valueOf(unit);
			if (tmpB < 0 && tmpB > 9999999) {
				errors.add(new ValidationError("birthplaceId", Messages
						.get("details.unit.range")));
			}
		}
		if (StringUtils.isNotEmpty(quantity)) {
			Integer tmpB = Integer.valueOf(quantity);
			if (tmpB < 0 && tmpB > 9999999) {
				errors.add(new ValidationError("birthplaceId", Messages
						.get("details.quantity.range")));
			}
		}

		if (errors.size() > 0) {
			System.out.println("ActorForm#validate errors");
			return errors;
		}

		return null;
	}

	@Override
	public String toString() {
		return "Details [ordernumber=" + ordernumber + ", product="
				+ product + ", unit=" + unit
				+ ",quantity=" + quantity + "]";
	}
}