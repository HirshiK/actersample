package views.form;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import play.data.validation.ValidationError;
import play.i18n.Messages;

public class ReceivedForm {
	public String ordernumber = "";
	public String customernumber = "";
	public String employeenumber = "";
	public String ordersdate = "";
	public String delivery = "";
	public String fare = "";

	public ReceivedForm() {

	}

	public ReceivedForm(String ordernumber, String customernumber,
			String employeenumber, String ordersdate, String delivery,
			String fare) {
		this.ordernumber = ordernumber;
		this.customernumber = customernumber;
		this.employeenumber = employeenumber;
		this.ordersdate = ordersdate;
		this.delivery = delivery;
		this.fare = fare;
	}

	public List<ValidationError> validate() {
		List<ValidationError> errors = new ArrayList<ValidationError>();

		if (ordernumber == null || ordernumber.length() == 0) {
			errors.add(new ValidationError("ordernumber", Messages
					.get("received.ordernumber.required")));
		}
		if (customernumber == null || customernumber.length() == 0) {
			errors.add(new ValidationError("height", Messages
					.get("received.customernumber.required")));
		}
		if (employeenumber == null || employeenumber.length() == 0) {
			errors.add(new ValidationError("employeenumber", Messages
					.get("received.employeenumber.required")));
		}
		if (StringUtils.isNotEmpty(ordersdate)
				&& !ordersdate.matches("\\d{4}-\\d{2}-\\d{2}")) {
			errors.add(new ValidationError("birthday", Messages
					.get("received.ordersdate.pattern")));
		}
		if (StringUtils.isNotEmpty(delivery)
				&& !delivery.matches("\\d{4}-\\d{2}-\\d{2}")) {
			errors.add(new ValidationError("delivery", Messages
					.get("received.delivery.pattern")));
		}
		if (fare == null || fare.length() == 0) {
			errors.add(new ValidationError("fare", Messages
					.get("received.fare.required")));
		}

		if (errors.size() > 0) {
			System.out.println("ActorForm#validate errors");
			return errors;
		}

		return null;
	}

	@Override
	public String toString() {
		return "Received [ordernumber=" + ordernumber + ", customernumber="
				+ customernumber + ", employeenumbert=" + employeenumber
				+ ", ordersdate=" + ordersdate + ",deliveryt=" + delivery
				+ ",fare=" + fare + "]";
	}
}
