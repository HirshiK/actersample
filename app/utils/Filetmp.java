package utils;

import java.io.File;
import java.io.IOException;

import play.Play;

public class Filetmp {

	//scalaから来た画像をtmpフォルダに配置する

	static public File filetmp(play.mvc.Http.MultipartFormData.FilePart picture,String str) throws IOException{

		String fileName = picture.getFilename()+"_0"+str;
		String contentType = picture.getContentType();
		File file = (File)picture.getFile();
		String fullPath = null;
		File upfile = null;
		System.out.println(fileName+"ファイルの大きさ："+file.length());
		if ( contentType.equals("image/jpeg") ) {
	    	  fullPath = Play.application().path().getPath() + "/public/images/upload/";
	    	  file.renameTo(new File(fullPath, fileName));
	    	  upfile = new File(fullPath+fileName);
	      }

		return upfile;
	}
}
