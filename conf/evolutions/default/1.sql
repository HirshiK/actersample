# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table received (
  ordernumber                   bigint(10) auto_increment not null,
  customernumber                varchar(255) not null,
  employeenumber                varchar(255) not null,
  ordersdate                    timestamp not null,
  delivery                      timestamp not null,
  fare                          varchar(255) not null,
  constraint pk_received primary key (ordernumber)
);

create table parent (
  id                            bigint not null,
  name                          varchar(255),
  create_date                   timestamp not null,
  update_date                   timestamp not null,
  constraint pk_parent primary key (id)
);
create sequence parent_seq;

create table actor (
  id                            bigint auto_increment not null,
  name                          varchar(30) not null,
  height                        integer,
  blood                         varchar(255),
  birthday                      timestamp,
  birthplace_id                 integer,
  seibetsu                      varchar(255),
  update_at                     timestamp not null,
  constraint pk_actor primary key (id)
);

create table customer (
  customernumber                bigint(10) auto_increment not null,
  companyname                   varchar(255) not null,
  parsonname                    varchar(255) not null,
  zipcode                       varchar(255) not null,
  address                       varchar(255) not null,
  phonenumber                   varchar(11),
  constraint pk_customer primary key (customernumber)
);

create table details (
  ordernumber                   bigint(10) auto_increment not null,
  product                       varchar(255) not null,
  unit                          integer not null,
  quantity                      integer not null,
  constraint pk_details primary key (ordernumber)
);

create table prefecture (
  id                            integer not null,
  name                          varchar(6) not null,
  constraint pk_prefecture primary key (id)
);
create sequence prefecture_seq;


# --- !Downs

drop table if exists received;

drop table if exists parent;
drop sequence if exists parent_seq;

drop table if exists actor;

drop table if exists customer;

drop table if exists details;

drop table if exists prefecture;
drop sequence if exists prefecture_seq;

